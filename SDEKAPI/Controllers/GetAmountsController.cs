﻿using Microsoft.AspNetCore.Mvc;
using SDEKAPI.Configuration;
using SDEKAPI.Models;
using SDEKAPI.Services;
using System.Linq;
using System.Text.Json;

namespace SDEKAPI.Controllers
{
    /// <summary>
    /// Контроллер для запросов по адресу /GetAmount
    /// </summary>
    [Route("[controller]")]
    [ApiController]
    public class GetAmountsController : ControllerBase
    {
        /// <summary>
        /// POST-запрос, который принимает JSON в виде объекта модели и возвращает значения в виде объекта модели
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        [HttpPost]
        public async Task<ResponseTariffListModel> Post([FromBody] TarifflistResponseModel value)
        {
            ResponseTariffListModel resp = new ResponseTariffListModel();

            DictionariesOperator dictionaryOperator = new DictionariesOperator();

            RequestFields Fields = new RequestFields();

            string firstCode = value.FromLocation.Code.ToString();
            string secondCode = value.ToLocation.Code.ToString();

            string? firstSDEKCode = await RedisInteraction.ReturnFromTheKeyValueAsync(firstCode);
            string? secondSDEKCode = await RedisInteraction.ReturnFromTheKeyValueAsync(secondCode);


            Thread thread1 = new Thread(async () =>
            {
                if (!TokensOperator.CheckTheValidity())
                {
                    await TokensOperator.SetToken();

                    await dictionaryOperator.FillDictionaryAsync();
                    await dictionaryOperator.GetDictionaryFromDataBaseAsync();
                    await dictionaryOperator.SetDictionaryToDataBaseAsync();
                    
                }

            });

            thread1.Start();

            if (firstSDEKCode != null && secondSDEKCode != null)
            {
                Fields.FromLocation = int.Parse(firstSDEKCode);
                Fields.ToLocation = int.Parse(secondSDEKCode);

                var responceTariffList = await RequestsSender.GetCostsAsync(value.Packages[0].Height,
                    value.Packages[0].Length, value.Packages[0].Weight, value.Packages[0].Width,
                    TokenJwt.AccessToken, (int)Fields.FromLocation, (int)Fields.ToLocation, SDEKAPIEndpoints.tarifflist);

                if (responceTariffList.Count > 0)
                {

                    ResponseTariffCodesModel[] mass = new ResponseTariffCodesModel[responceTariffList.Count];
                    int j = 0;
                    foreach (var item in responceTariffList)
                    {
                        mass[j] = new ResponseTariffCodesModel()
                        {
                            DeliverySum = item.Value
                        };
                        j++;
                        resp = new ResponseTariffListModel()
                        {
                            Tariffs = mass
                        };
                    }

                }
                else
                {
                    throw new Exception("Извините, сервис в данный момент загружен");
                }
                
            }
            return resp;

        }
    }
}
