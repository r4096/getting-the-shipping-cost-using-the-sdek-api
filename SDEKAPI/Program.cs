using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SDEKAPI.Configuration;
using SDEKAPI.Models;
using SDEKAPI.Services;

namespace SDEKAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {

            Thread thread1 = new Thread(() =>
            {
                var host = CreateWebHostBuilder(args).Build();
                host.Run();

            });
            Thread thread2 = new Thread(async () =>
            {
                await Initialize.Instance.InitializeDateBase();
            });

            thread1.Start();
            thread2.Start();

        }

        public static IHostBuilder CreateWebHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}