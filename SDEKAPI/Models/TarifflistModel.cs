﻿using System.Text.Json.Serialization;

namespace SDEKAPI.Models
{
    /// <summary>
    /// Модель для экземпляра JSON из модели
    /// </summary>
    public class TariffListModel
    {
        [JsonPropertyName("tariff_codes")]
        public TariffCodesModel[]? TariffCodes { get; set; }
    }

    public class TariffCodesModel
    {
        [JsonPropertyName("tariff_name")]
        public string? TariffName { get; set; }
        [JsonPropertyName("delivery_sum")]
        public double? DeliverySum { get; set; }
    }
}
