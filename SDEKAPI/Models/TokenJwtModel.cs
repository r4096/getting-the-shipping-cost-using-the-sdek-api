﻿using System.Text.Json.Serialization;

namespace SDEKAPI.Models
{
    /// <summary>
    /// Модель для экземпляра токена приложения
    /// </summary>
    public class TokenJwtModel
    {
        [JsonPropertyName("access_token")]
        public string? AccessToken { get; set; }
    }
}
