﻿using System.Text.Json.Serialization;

namespace SDEKAPI.Models
{
    /// <summary>
    /// Модель для экземпляра JSON из модели
    /// </summary>
    public class TarifflistBodyModel
    {
        [JsonPropertyName("type")]
        public int? Type { get; set; }

        [JsonPropertyName("from_location")]
        public CodesModel? FromLocation { get; set; }

        [JsonPropertyName("to_location")]
        public CodesModel? ToLocation { get; set; }

        [JsonPropertyName("packages")]
        public List<PackagesModel>? Packages { get; set; }
    }

    public class CodesModel
    {
        [JsonPropertyName("code")]
        public int? Code { get; set; }
    }

    public class PackagesModel
    {
        [JsonPropertyName("height")]
        public int Height { get; set; }
        [JsonPropertyName("length")]
        public int Length { get; set; }
        [JsonPropertyName("weight")]
        public int Weight { get; set; }
        [JsonPropertyName("width")]
        public int Width { get; set; }
    }
}
