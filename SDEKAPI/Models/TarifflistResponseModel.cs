﻿using System.Text.Json.Serialization;

namespace SDEKAPI.Models
{
    /// <summary>
    /// Модель для экземпляра JSON из модели
    /// </summary>
    public class TarifflistResponseModel
    {
        [JsonPropertyName("type")]
        public int? Type { get; set; }

        [JsonPropertyName("from_location")]
        public required CodesResponseModel FromLocation { get; set; }

        [JsonPropertyName("to_location")]
        public required CodesResponseModel ToLocation { get; set; }

        [JsonPropertyName("packages")]
        public required List<PackagesResponseModel> Packages { get; set; }
    }

    public class CodesResponseModel
    {
        [JsonPropertyName("code")]
        public long Code { get; set; }
    }

    public class PackagesResponseModel
    {
        [JsonPropertyName("height")]
        public int Height { get; set; }
        [JsonPropertyName("length")]
        public int Length { get; set; }
        [JsonPropertyName("weight")]
        public int Weight { get; set; }
        [JsonPropertyName("width")]
        public int Width { get; set; }
    }
}
