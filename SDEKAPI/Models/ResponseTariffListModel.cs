﻿using System;
using System.Text.Json.Serialization;

namespace SDEKAPI.Models
{
    /// <summary>
    /// Модель для экземпляра JSON из модели
    /// </summary>
    public class ResponseTariffListModel
    {
        [JsonPropertyName("tariffs")]
        public ResponseTariffCodesModel[] Tariffs { get; set; }
    }

    public class ResponseTariffCodesModel
    {
        [JsonPropertyName("delivery_sum")]
        public double DeliverySum { get; set; }
    }
}
