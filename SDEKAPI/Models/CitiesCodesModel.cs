﻿using System.Text.Json.Serialization;

namespace SDEKAPI.Models
{
    /// <summary>
    /// Модель для экземпляра JSON из модели
    /// </summary>
    public class CitiesCodesModel
    {
        [JsonPropertyName("code")]
        public int Code { get; set; }
        [JsonPropertyName("kladr_code")]
        public string? KladrCode { get; set; }
    }
}
