﻿namespace SDEKAPI.Configuration
{
    /// <summary>
    /// Поля конфигурации токена приложения
    /// </summary>
    static class TokenJwt
    {
        public static string? AccessToken { get; set; }
        public static DateTime? TimeReceipt { get; set; }
        public static readonly TimeSpan Lifecycle = new TimeSpan(0, 0, 3500);
    }
}