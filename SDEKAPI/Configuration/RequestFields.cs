﻿using SDEKAPI.Models;
namespace SDEKAPI.Configuration

{
    /// <summary>
    /// Поля конфигурации для запроса стоимости доставки
    /// </summary>
    public class RequestFields
    {
        public int? FromLocation { get; set; }
        public int? ToLocation { get; set; }
        public static readonly int Type = 2;
    }
}
