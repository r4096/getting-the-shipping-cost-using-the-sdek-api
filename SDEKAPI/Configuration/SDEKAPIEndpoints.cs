﻿namespace SDEKAPI.Configuration
{
    /// <summary>
    /// Поля конфигурации сетевых адресов SDEK API
    /// </summary>
    public static class SDEKAPIEndpoints
    {
        public static readonly string token = "https://api.edu.cdek.ru/v2/oauth/token";
        public static readonly string cities = "https://api.edu.cdek.ru/v2/location/cities";
        public static readonly string tarifflist = "https://api.edu.cdek.ru/v2/calculator/tarifflist";
    }
}
