﻿namespace SDEKAPI.Configuration
{
    /// <summary>
    /// Поля конфигурации подключения к Redis
    /// </summary>
    static class RedisConfiguration
    {
        public static readonly string Endpoint = "localhost";
        public static readonly int Port = 6379;
        public static readonly int AsyncTimeout = 60000;
        public static readonly string isDatabaseEmptyField = "isDatabaseEmpty";

    }
}
