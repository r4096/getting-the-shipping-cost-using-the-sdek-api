﻿using SDEKAPI.Configuration;
namespace SDEKAPI.Services
{
    /// <summary>
    /// Класс для операций над словарем
    /// </summary>
    public class DictionariesOperator
    {
        /// <summary>
        /// Собственный словарь класса
        /// </summary>
        private Dictionary<string, int> FiosSDEKCodes { get; set; } = new Dictionary<string, int> { };
        
        /// <summary>
        /// Заполнить словарь парами ФИАС-Код СДЭК
        /// </summary>
        public async Task FillDictionaryAsync()
        {
            if (TokenJwt.AccessToken != null)
                FiosSDEKCodes = await RequestsSender.GetADictionaryOfCityCodesAsync(TokenJwt.AccessToken, SDEKAPIEndpoints.cities);
        }

        /// <summary>
        /// Получить все ключи словаря, которых нет в Redis
        /// </summary>
        public async Task GetDictionaryFromDataBaseAsync()
        {            
            FiosSDEKCodes = await RedisInteraction.ReturnFromTheDictionarySuperfluousAsync(FiosSDEKCodes);
        }
        
        /// <summary>
        /// Установить пл ключам словаря значения Redos
        /// </summary>
        public async Task SetDictionaryToDataBaseAsync() =>
            await RedisInteraction.InsertFromIDictAsync(FiosSDEKCodes);
    }
}
