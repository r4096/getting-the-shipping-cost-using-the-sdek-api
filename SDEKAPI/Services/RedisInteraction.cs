﻿using StackExchange.Redis;
using SDEKAPI.Configuration;

namespace SDEKAPI.Services
{
    /// <summary>
    /// Сервис для взаимодействия с Redis
    /// </summary>
    static class RedisInteraction
    {
        /// <summary>
        /// Конфигурация Redis
        /// </summary>
        readonly static ConfigurationOptions config = new ConfigurationOptions
        {
            EndPoints =
            {
                {
                    RedisConfiguration.Endpoint,
                    RedisConfiguration.Port
                }
            },
            AsyncTimeout = RedisConfiguration.AsyncTimeout,
        };

        /// <summary>
        /// Вставить пары keys-values из IDictionary в Redis
        /// </summary>
        /// <typeparam name="T">Первый аргумент notnull</typeparam>
        /// <typeparam name="K">Второй аргумент notnull</typeparam>
        /// <param name="dict">IDictionary</param>
        public static async Task InsertFromIDictAsync<T, K>(IDictionary<T, K> dict)
            where T : notnull
            where K : notnull
        {
            if (dict.Count > 0)
                using (ConnectionMultiplexer redis = ConnectionMultiplexer.Connect(config))
                {

                    var db = redis.GetDatabase();

                    foreach (var dictIter in dict)
                    {
                        await db.StringAppendAsync(dictIter.Key.ToString(), dictIter.Value.ToString());
                    }

                    await db.StringAppendAsync(RedisConfiguration.isDatabaseEmptyField, "1");
                }
        }

        /// <summary>
        /// Вернуть из словаря ключи, которых нет в Redis в виде словаря
        /// </summary>
        /// <typeparam name="T">Первый аргумент notnull</typeparam>
        /// <typeparam name="K">Второй аргумент notnull</typeparam>
        /// <param name="dict">IDictionary</param>
        public static async Task<Dictionary<T, K>> ReturnFromTheDictionarySuperfluousAsync<T, K>(IDictionary<T, K> dict)
            where T : notnull
            where K : notnull
        {
            using (ConnectionMultiplexer redis = ConnectionMultiplexer.Connect(config))
            {
                var db = redis.GetDatabase();

                Dictionary<T, K> newDict = new Dictionary<T, K> { };

                foreach (var dictIter in dict)
                {
                    if (!await db.KeyExistsAsync(dictIter.Key.ToString()))
                    {
                        newDict.Add(dictIter.Key, dictIter.Value);
                    }

                }
                return newDict;
            }
        }

        /// <summary>
        /// Проверка на то, пустая ли БД Redis
        /// </summary>
        /// <returns>bool</returns>
        public static async Task<bool> FindOutIfTheDatabaseIsEmptyAsync()
        {
            using (ConnectionMultiplexer redis = ConnectionMultiplexer.Connect(config))
            {
                var db = redis.GetDatabase();

                if (await db.KeyExistsAsync(RedisConfiguration.isDatabaseEmptyField))
                {
                    return false;
                }

                return true;
            }
        }

        /// <summary>
        /// Вернуть по ключу значение
        /// </summary>
        /// <typeparam name="T">Первый агрумент notnull</typeparam>
        /// <param name="Key">Аргумент поиска</param>
        /// <returns>string?</returns>
        public static async Task<string?> ReturnFromTheKeyValueAsync<T>(T Key)
            where T : notnull
        {
            using (ConnectionMultiplexer redis = ConnectionMultiplexer.Connect(config))
            {
                var db = redis.GetDatabase();

                if (await db.KeyExistsAsync(Key.ToString()))
                {
                    return (string?)await db.StringGetAsync(Key.ToString());
                }

            }
            return null;
        }
    }

}