﻿using SDEKAPI.Models;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Text;

namespace SDEKAPI.Services
{
    static class RequestsSender
    {
        /// <summary>
        /// Получить токен от сервиса
        /// </summary>
        /// <param name="url">Endpoint</param>
        /// <returns>tokem string?</returns>
        public static async Task<string?> GetJWTTokenAsync(string url)
        {

            using (var client = new HttpClient())
            {
                // string url = "https://api.edu.cdek.ru/v2/oauth/token";
                var dict = new Dictionary<string, string>();
                dict.Add("grant_type", "client_credentials");
                dict.Add("client_id", "EMscd6r9JnFiQ3bLoyjJY6eM78JrJceI");
                dict.Add("client_secret", "PjLZkKBHEiLK3YsjtNrt3TGNG0ahs3kG");

                var response = await client.PostAsync(url, new FormUrlEncodedContent(dict));

                var responceContentAsStream = await response.Content.ReadAsStreamAsync();
                var options = new JsonSerializerOptions(JsonSerializerDefaults.Web);
                var tokenJwtObject = await JsonSerializer.DeserializeAsync<TokenJwtModel>(responceContentAsStream, options);

                return tokenJwtObject?.AccessToken;
            }
        }
        /// <summary>
        /// Получение словаря ФИАС-Код СДЭК
        /// </summary>
        /// <param name="Token">Токен приложения</param>
        /// <param name="endpoint">Endpoint</param>
        /// <returns>Dictionary<string, int></returns>
        public static async Task<Dictionary<string, int>> GetADictionaryOfCityCodesAsync(string Token,
            string endpoint)
        {
            Dictionary<string, int> FiasSDEKCodes = new Dictionary<string, int> { };
            using (var client = new HttpClient())
            {
                for (int i = 0; i < 200; i++)
                {

                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                    string url = $"{endpoint}?page={i}&size=10000";
                    try
                    {

                        var response = await client.GetAsync(url);

                        var headers = response.Content.Headers.ContentLength;

                        if (headers > 2)
                        {

                            List<CitiesCodesModel>? model = null;

                            var responceContent = response.Content;

                            var StreamJSON = await responceContent.ReadAsStreamAsync();

                            model = await JsonSerializer.DeserializeAsync<List<CitiesCodesModel>>(StreamJSON);
                            if (model != null)
                                foreach (CitiesCodesModel cityCode in model)
                                {
                                    if (cityCode.KladrCode != null)
                                    {
                                        FiasSDEKCodes.Add(cityCode.KladrCode, cityCode.Code);
                                    }
                                }
                        }
                        else
                        {
                            break;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        Console.WriteLine(ex.Data);
                    }

                }
            }
            return FiasSDEKCodes;
        }
        /// <summary>
        /// Получение словаря стоимости
        /// </summary>
        /// <param name="height">Высота груза, мм</param>
        /// <param name="length">Длина груза, мм</param>
        /// <param name="weight">Вес груза, г</param>
        /// <param name="width">Ширина груза, мм</param>
        /// <param name="Token">Токен приложения</param>
        /// <param name="FromLocation">Откуда</param>
        /// <param name="ToLocation">Куда</param>
        /// <param name="url">Endpoint</param>
        /// <returns>Dictionary<string, double></returns>
        public static async Task<Dictionary<string, double>> GetCostsAsync(int height, int length,
            int weight, int width, string Token, int FromLocation, int ToLocation, string url)
        {
            Dictionary<string, double> TariffsFields = new Dictionary<string, double> { };

            using (var client = new HttpClient())
            {
                var modelForJSON = new TarifflistBodyModel
                {
                    Type = 2,
                    FromLocation = new CodesModel { Code = FromLocation },
                    ToLocation = new CodesModel { Code = ToLocation },
                    Packages = new List<PackagesModel> {
                    new PackagesModel
                    {
                        Height = height,
                        Length = length,
                        Weight = weight,
                        Width = width
                    }
                }

                };

                var json = JsonSerializer.Serialize(modelForJSON);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);

                try
                {

                    var response = await client.PostAsync(url, content);

                    var headers = response.Content.Headers.ContentLength;

                    if (headers > 2)
                    {

                        var responceContent = response.Content;

                        var StreamJSON = await responceContent.ReadAsStringAsync();

                        var model = JsonSerializer.Deserialize<TariffListModel>(StreamJSON);

                        if (model != null && model?.TariffCodes != null)
                        {
                            foreach (var namesAndSumms in model.TariffCodes)
                            {
                                if (namesAndSumms.TariffName != null && namesAndSumms.DeliverySum != null)
                                {
                                    if (namesAndSumms.TariffName.Contains("дверь-дверь"))
                                    {
                                        TariffsFields.Add(namesAndSumms.TariffName, (double)namesAndSumms.DeliverySum);
                                    }
                                }
                            }

                        }

                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            return TariffsFields;
        }

    }

}
