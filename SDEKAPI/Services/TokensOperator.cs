﻿using SDEKAPI.Configuration;
namespace SDEKAPI.Services
{
    public static class TokensOperator
    {
        /// <summary>
        /// Установить новый токен в приложении
        /// </summary>
        public static async Task SetToken()
        {
            string? token = await RequestsSender.GetJWTTokenAsync(
                SDEKAPIEndpoints.token);
            if (token != null)
            {
                TokenJwt.AccessToken = token;
                TokenJwt.TimeReceipt = DateTime.Now;
            }
        }
        /// <summary>
        /// Проверка валидности токена
        /// </summary>
        public static bool CheckTheValidity()
        {
            if (TokenJwt.AccessToken == null || TokenJwt.TimeReceipt == null
                || TokenJwt.TimeReceipt + TokenJwt.Lifecycle < (DateTime.Now))
            {
                return false;
            }
            return true;
        }
    }
}