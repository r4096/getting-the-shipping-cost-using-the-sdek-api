﻿namespace SDEKAPI.Services
{
    public interface IInitialize
    {
        Task InitializeDateBase();
    }
    public class Initialize : IInitialize
    {
        private static readonly Initialize _instance = new Initialize();
        public static Initialize Instance { get; } = _instance;
        public Initialize()
        {
        }
        /// <summary>
        /// Инициализация приложения
        /// </summary>
        public async Task InitializeDateBase()
        {
            await TokensOperator.SetToken();
            bool isTheDatabaseIsEmpty = await RedisInteraction.FindOutIfTheDatabaseIsEmptyAsync();
            if (isTheDatabaseIsEmpty)
            {
                DictionariesOperator dictionaryOperator = new DictionariesOperator();
                await dictionaryOperator.FillDictionaryAsync();
                await dictionaryOperator.GetDictionaryFromDataBaseAsync();
                await dictionaryOperator.SetDictionaryToDataBaseAsync();
            }
        }
    }
}
