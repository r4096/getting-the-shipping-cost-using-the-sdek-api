# Getting the shipping cost using the SDEK API

## Задача
Разработка WEB API приложения на .Net для взаимодействия с SDEK API

Приложение будет принимать следующие параметры:

|  Параметр  | Измерение |
| -------- | ------ |
|    Вес   |  Граммы      |
| Габариты |  Миллиметры      |
| Город отправления         |  ФИАС-код      |
| Город получения         |  ФИАС-код      |

Расчет нужно осуществлять при следующих условиях: одноместное отправление курьерской доставки с
использованием метода “расчет стоимости по тарифам без
приоритета”

На выходе - отдавать стоимости доставки с помощью транспортной
компании СДЭК груза

## Запуск

Для запуска Redis запустите docker-compose.yml файл в папке приложения с помощью команды:

```

docker-compose up --build -d


```

Для запуска .Net приложения запустите Startup.cs

## Куда и как кидать запрос

Приложение принимает POST-запрос по следующему адресу:
`http://localhost:IP/GetAmounts`  


Принимает в теле запроса JSON следующего формата:

```
{
    "from_location": {
        "code": 7700000004000
    },
    "to_location": {
        "code": 3900000100000
    },
    "packages": [
        {
            "height": 10,
            "length": 10,
            "weight": 400,
            "width": 10
        }
    ]
}

```
,где _from_location_ и _to_location_ - фиас коды города отправления и получения соответственно,
_height_, _length_ и _width_ - высота, длина и ширина посылки в миллиметрах, а _weight_ - вес посылки в граммах


Ответ сервиса выглядит следующим образом:

```
{
    "tariffs": [
        {
            "delivery_sum": 740
        },
        {
            "delivery_sum": 2130
        }
    ]
}

```
,где delivery_sum - суммы способов отправки, соответсвующие условиям задачи и заданным параметрам

## Как был выбран метод расчета?


Одноместное отправление курьерской доставки с
использованием метода “расчет стоимости по тарифам без
приоритета” означает, что необходимо отказаться от параметра type = 1 в запросе стоимости тарифа, так как это означает интернет-магазин. 

Значит нужно выбрать type = 2, "доставка", потому что при доставке курьер не приносит товар из интернет-магазина, а только доставляет груз от отправителя к получателю.

"Расчет стоимости по тарифам без приоритета" означает "Расчет по доступным тарифам",  а не "Расчет по коду тарифа"

В параметрах имени тарифа есть следующие параметры:
| параметр | значение |
| ------ | ------ |
| Д-Д       | Дверь-Дверь       |
| С-Д       | Склад-Дверь       | 
| Д-С       | Дверь-Склад       |
| С-С       | Склад-Склад       | 

Способ доставки “Д-Д”, то есть “Дверь-Дверь”, подходит для курьерской одноместной доставки. В этом случае отправитель и получатель находятся по разным адресам, и доставка осуществляется от двери отправителя до двери получателя одним курьером.


Таким образом, параметры будут следующими: метод "калькулятор.Расчет по доступным тарифам", type = 2, а теле ответа отсеять все варианты тарифов, кроме типа Д-Д


## Принципы работы

#### Ниже представлена блок-схема логики

<picture>
 <img alt="Принцип работы" src="Principles.png">
</picture>

Заполнение Redis, работа с IDictionary, запросами и прочим разделена на сервисы, экземпляры JSON для запросов производятся из моделей, а поля настройки конфигурации вынесены в папку конфигураций

При первичной инициализации в Redis ещё нет никаких значений. Поэтому понадобится время для заполнения этих значений. Как только они будут заполнены, перепроверка значений раз в час позволяет своевременно добавлять себе ключи, по мере добавления городов со стороны SDEK API.
В данном случае был создал Docker том, куда Redis кеширует свои значения и сможет ими оперировать даже после полного перезапуска контейнера

Запросы и заполнение данных происходят асинхронно и на разных потоках, что позволяет обрабатывать запросы без ожидания друг друга

Сервис, с которым мы взаимодействуем, защищен токеном JWT, что означает, что при каждом запросе нам нужно передавать его, а также токен обладает экспирацией и, следовательно, он нуждается в обновлении. исключением является адрес получения токена для тестовой учетной записи. 
Реквизиты тестовой записи следующие:

| Параметр      | Значение      |
| ------      | ------      |
|   Account     |   EMscd6r9JnFiQ3bLoyjJY6eM78JrJce     |
|   Secure password    |   PjLZkKBHEiLK3YsjtNrt3TGNG0ahs3kG     |

Для получения стоимости расчета необходим код города СДЭК, который прямым путем, по параметрам, получить нельзя - метод является устаревшим

Поэтому в приложении реализована загрузка сопоставлений кода ФИАС и кода СДЭК в словарь, а затем передача его в Redis для сохранения его в виде файла дампа на диске и производительной работы
